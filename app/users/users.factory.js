(function() {

    'use strict';

    module.exports = function usersFctr($resource) {
        var path = 'http://localhost:8080/APBackend/api/users';
        var users = $resource(path + '?:param/:id',
            {
                id: '@id',
                param: '@param'
            },
            {
                'query':  {method: 'GET', isArray: true},/*Get all users.*/
                'get':    {method: 'GET'},               /*Get a single user.*/
                'update': {method: 'PUT'},               /*Update a single user.*/
                'save': {method: 'POST'},                /*Create a new user.*/
                'delete': {method: 'DELETE'}             /*Delete a single user.*/
            }
        );
        return users;
    };
})();
