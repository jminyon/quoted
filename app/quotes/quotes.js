(function() {

'use strict';

var app = angular.module('quotes', [
    'ngCookies'
]);

app.controller('quotesCtrl', require('./quotes.controller'))
.controller('editQuoteCtrl', require('./editQuote.controller'))
.controller('newQuoteCtrl', require('./newQuote.controller'))
.service('quotesSrvc', require('./quotes.service'));

})();
