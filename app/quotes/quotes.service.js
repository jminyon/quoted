(function() {

    'use strict';

    module.exports = function quotesSrvc(usersFctr, quotesFctr) {
        this.getQuotes = function() {
            quotesFctr.query(
                function(data) {
                    return data;
                },
                function(error) {
                    console.log('ERROR FETCHING QUOTES');
                }
            );
        };
        this.getUsers = function(id) {
            usersFctr.get(id)
                .then(
                function(data) {
                    return data;
                },
                function(error) {
                    console.log('ERROR FETCHING USERS');
                }
                );
        };
    };

})();
