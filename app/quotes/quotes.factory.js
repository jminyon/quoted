(function() {

    'use strict';

    module.exports = function quotesFctr($resource) {
        var url = 'http://localhost:8080/APBackend/api/quotes/';
        var quotes = $resource(url + ':id',
            {
                id: '@id'
            },
            {
                'query': {method: 'GET', isArray: true},
                'get': {method: 'GET'},
                'save': {method: 'POST'},
                'update': {method: 'POST'},
                'delete': {method: 'DELETE'}
            }
        );
        return quotes;
    };
})();
