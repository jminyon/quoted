(function() {

'use strict';

var app = angular.module('shared', [
    'ngResource'
]);

app.factory('quotesFctr', require('../quotes/quotes.factory'))
.factory('usersFctr', require('../users/users.factory'));

})();
